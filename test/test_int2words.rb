#!/usr/bin/env ruby

$: << '../lib'

require 'test/unit'
require 'int2words'

class Int2wordsTest < Test::Unit::TestCase

  def test_toobig
    a = (10**100).to_words
    assert_equal ["Too Big"], a
  end

  def test_zero
    a = 0.to_words
    assert_equal ["Zero"], a
  end

  def test_one
    a = 1.to_words
    assert_equal ["One"], a
  end

  def test_10
    a = 10.to_words
    assert_equal ["Ten"], a
  end

  def test_100
    a = 100.to_words
    assert_equal ["One hundred"], a
  end

  def test_by_thousands
    results = {
      1_000 => "One thousand",
      1_000_000 => "One million",
      1_000_000_000 => "One billion",
      1_000_000_000_000 => "One trillion",
      1_000_000_000_000_000 => "One quadrillion",
      1_000_000_000_000_000_000 => "One quintillion",
      1_000_000_000_000_000_000_000 => "One sextillion",
      1_000_000_000_000_000_000_000_000 => "One septillion",
      1_000_000_000_000_000_000_000_000_000 => "One octillion",
      1_000_000_000_000_000_000_000_000_000_000 => "One nonillion",
      1_000_000_000_000_000_000_000_000_000_000_000 => "One decillion",
      1_000_000_000_000_000_000_000_000_000_000_000_000 => "One undecillion",
      1_000_000_000_000_000_000_000_000_000_000_000_000_000 => "One duodecillion",
      1_000_000_000_000_000_000_000_000_000_000_000_000_000_000 => "One tredecillion",
      1_000_000_000_000_000_000_000_000_000_000_000_000_000_000_000 => "One quattuordecillion",
      1_000_000_000_000_000_000_000_000_000_000_000_000_000_000_000_000 => "One quindecillion",
      1_000_000_000_000_000_000_000_000_000_000_000_000_000_000_000_000_000 => "One sexdecillion",
      1_000_000_000_000_000_000_000_000_000_000_000_000_000_000_000_000_000_000 => "One septendecillion",
      1_000_000_000_000_000_000_000_000_000_000_000_000_000_000_000_000_000_000_000 => "One octodecillion",
      1_000_000_000_000_000_000_000_000_000_000_000_000_000_000_000_000_000_000_000_000 => "One novemdecillion",
      1_000_000_000_000_000_000_000_000_000_000_000_000_000_000_000_000_000_000_000_000_000 => "One vigintillion"
    }
    i = 1000
    while ( i < 10**63 ) do
      i *= 1000
      a = i.to_words
      assert_equal [results[i]], a
    end
  end

  def test_random
    results = {
      1_505_390_248_594_782 => [
        "One quadrillion",
        "Five hundred Five trillion",
        "Three hundred Ninety billion",
        "Two hundred Forty-eight million",
        "Five hundred Ninety-four thousand",
        "Seven hundred Eighty-two",
      ],
      1_234 => [
        "One thousand",
        "Two hundred Thirty-four"
      ],
      97 => ["Ninety-seven"],
      123_456_789 => [
        "One hundred Twenty-three million",
        "Four hundred Fifty-six thousand",
        "Seven hundred Eighty-nine"
      ],
      123_456_789_012 => [
        "One hundred Twenty-three billion",
        "Four hundred Fifty-six million",
        "Seven hundred Eighty-nine thousand",
        "Twelve"
      ],
      1_000_000_000_001 => [
        "One trillion",
        "One"
      ]
    }
    results.keys.each do |num|
      a = num.to_words
      assert_equal results[num], a
    end
  end

end
